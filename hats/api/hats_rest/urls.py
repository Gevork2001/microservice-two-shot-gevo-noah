from django.urls import path
from .views import(
    api_show_hats,
    api_hat_list,
)

urlpatterns = [
    path("hats/", api_hat_list, name='api_hat_list'),
    path('hats/<int:pk>/', api_show_hats, name="api_show_hats"),
    
]
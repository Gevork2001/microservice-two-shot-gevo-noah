import json
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.shortcuts import HttpResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder

from .models import Hats
from .models import LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "section_number",
        "shelf_number",
        "import_href",
        "closet_name",
    ]

class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
       "fabric",
       "style_name",
       "color",
       "id",
       "picture_url",
       "location",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_hat_list(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )        
    else: #POST
        content = json.loads(request.body)
        try:
            href = f'/api/locations/{content["location"]}/'
            location = LocationVO.objects.get(import_href=href)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid wardrobe location"},
                status=400,
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            {"hat" : hat},
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count , _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            hat = Hats.objects.get(id=pk)
            content = hat
        except Hats.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        Hats.objects.filter(id=pk).update(**content)
        conference = Hats.objects.get(id=pk)
        return JsonResponse(
            conference,
            encoder=HatDetailEncoder,
            safe=False,
        )
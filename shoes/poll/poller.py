import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # Write your polling logic, here
            res = requests.get('http://wardrobe-api:8000/api/bins/')
            data = json.loads(res.content)
            for bin in data["bins"]:
                BinVO.objects.update_or_create(
                    import_href=bin['href'],
                    defaults={
                        "bin_number": bin["bin_number"],
                        "closet_name": bin["closet_name"],
                        "bin_size": bin["bin_size"],
                    }


                )
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()

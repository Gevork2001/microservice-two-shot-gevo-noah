from django.db import models
from django.urls import reverse

# reate your models here.


class BinVO(models.Model):
    import_href = models.URLField(max_length=200, unique=True)
    bin_number = models.CharField(max_length=100)
    closet_name = models.CharField(max_length=200, null=True)
    bin_size = models.IntegerField(null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField()

    bin = models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.PROTECT,
        null=True
    )


def get_api_url(self):
    return reverse("api_list_shoes", kwargs={"pk": self.pk})


def __str__(self):
    return f"{self.manufacturer} - {self.model_name}/{self.color}/{self.color}"


class Meta:
    ordering = ("manufacturer", "model_name", "color")

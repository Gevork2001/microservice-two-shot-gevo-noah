import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatList from "./HatsList";
import HatForm from "./HatsForm";
import HatDelete from "./HatsList"; // You may need to verify if HatDelete should be used

import ShoeList from "./ShoeList";
import ShoeForm from "./ShoeForm";
import ShoeDelete from "./ShoeDelete";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats">
            <Route index element={<HatList />} />
            <Route path="new" element={<HatForm />} />
            <Route path="delete" element={<HatDelete />} /> {/* Verify if HatDelete should be used here */}
          </Route>
          <Route path="/shoes">
            <Route index element={<ShoeList />} />
            <Route path="new" element={<ShoeForm />} />
            <Route path="delete" element={<ShoeDelete />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

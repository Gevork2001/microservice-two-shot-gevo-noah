import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function ShoeColumn(props) {
  const shoeDelete = async (event) => {
    event.preventDefault();
    const id = event.target.value;
    const url = `http://localhost:8080/api/shoes/${id}/`;
    const fetchConfig = {
      method: "DELETE",
      body: "",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      console.log("Deleted shoe");
    } else {
      console.log("Could not delete shoe");
    }
  };

  return (
    <div className="col">
      {props.list.map((data) => {
        const shoe = data.shoe;
        return (
          <div key={shoe.id} className="card mb-3 shadow">
            <img
              src={shoe.picture_url}
              className="card-img-top"
              alt={`Shoe ${shoe.id}`}
            />
            <div className="card-body">
              <h5 className="card-title">Shoe {shoe.id}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                Manufacturer: {shoe.manufacturer}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                Model Name: {shoe.model_name}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                Color: {shoe.color}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                Bin:{" "}
                {shoe.bin
                  ? `${shoe.bin.closet_name} - Bin ${shoe.bin.bin_number}`
                  : "N/A"}
              </h6>
              <button
                onClick={shoeDelete}
                value={shoe.id}
                className="btn btn-info mx-1"
              >
                Update (Don't click)
              </button>
              <button
                onClick={shoeDelete}
                value={shoe.id}
                className="btn btn-danger mx-1"
              >
                Delete
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

const MainPage = (props) => {
  const [shoeColumns, setShoeColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const url = "http://localhost:8080/api/shoes/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        console.log(data);

        const requests = [];
        for (let shoe of data.shoe) {
          const detailUrl = `http://localhost:8080/api/shoes/${shoe.id}/`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);
        const columns = [[], [], []];

        let i = 0;
        for (const shoeResponse of responses) {
          if (shoeResponse.ok) {
            const details = await shoeResponse.json();
            columns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(shoeResponse);
          }
        }

        setShoeColumns(columns);
      }
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="top">
        <img
          className="bg-white rounded shadow d-block mx-auto mb-4"
          src="/logo.svg"
          alt=""
          width="600"
        />
        <h1 className="display-5 fw-bold">Wardrobify</h1>
        <div className="col-lg mx-auto">
          <p className="lead mb-4">Virtual closet to store your shoes</p>
          <div className="d-grid gap-4 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">
              Create a shoe
            </Link>
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">
              Update a shoe
            </Link>
            <Link
              to="/shoes/delete"
              className="btn btn-primary btn-lg px-4 gap-3"
            >
              Delete a shoe
            </Link>
          </div>
        </div>
      </div>
      <div className="container">
        <h2>Shoes in your closet</h2>
        <div className="row">
          {shoeColumns.map((shoeList, index) => {
            return <ShoeColumn key={index} list={shoeList} />;
          })}
        </div>
      </div>
    </>
  );
};

export default MainPage;

/*import React from "react";

function ShoeList(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Bin</th>
        </tr>
      </thead>
      <tbody>
        {props.shoes.map((shoe) => {
          return (
            <tr key={shoe.id}>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.color}</td>
              <td>
                <img
                  src={shoe.picture_url}
                  alt={`${shoe.manufacturer} - ${shoe.model_name}`}
                  width="50"
                  height="50"
                />
              </td>
              <td>
                {shoe.bin
                  ? `${shoe.bin.closet_name} - Bin ${shoe.bin.bin_number}`
                  : "N/A"}
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ShoeList;*/

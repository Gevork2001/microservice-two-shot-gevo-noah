import React, { useEffect, useState} from "react";
import { Link } from 'react-router-dom';
function HatColumn(props){
    const hatDelete = async (event) => {
        event.preventDefault();
        const id = event.target.value;
        const url = `http://localhost:8090/api/hats/${id}/`
        const fetchConfig = {
            method: "DELETE",
            body: "",
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok){
            console.log("Deleted hat");
        }else{
            console.log("Could not delete hat");
        }

    }

    return (
        <div className="col">
          {props.list.map(data => {
            const hat = data.hat;
            return (
              <div key={hat.id} className="card mb-3 shadow">
                <img src={hat.picture_url} className="card-img-top" />
                <div className="card-body">
                  <h5 className="card-title">Hat {hat.id}</h5>
                  <h6 className="card-subtitle mb-2 text-muted">
                    Fabric: {hat.fabric}
                  </h6>
                  <h6 className="card-subtitle mb-2 text-muted">
                    Closet: {hat.location.closet_name}
                  </h6>
                  <h6 className="card-subtitle mb-2 text-muted">
                    Color: {hat.color}
                  </h6>
                  <button onClick={hatDelete} value={hat.id} className="btn btn-info mx-1">Update (Don't click)</button>
                  <button onClick={hatDelete} value={hat.id} className="btn btn-danger mx-1">Delete</button>
                </div>
              </div>
            );
          })}
        </div>
      );
    }
  
  const MainPage = (props) => {
    const [hatColumns, setHatColumns] = useState([[],[],[]]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';

        try {
            const response = await fetch(url);
            if (response.ok){
                const data = await response.json();
                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090/api/hats/${hat.id}/`;
                    requests.push(fetch(detailUrl));
                }
                const responses = await Promise.all(requests);
                const columns = [[],[],[]];
                let i = 0;
                for (const hatResponse of response) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        columns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    }else {
                        console.error(hatResponse);
                    }
                }
                    setHatColumns(columns);
            }
        } catch (e) {
            console.error(e);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
          <div className="top">
            <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
            <h1 className="display-5 fw-bold">Wardrobify</h1>
            <div className="col-lg mx-auto">
              <p className="lead mb-4">
                Virtual closet to store your hats
              </p>
              <div className="d-grid gap-4 d-sm-flex justify-content-sm-center">
                <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Create a hat</Link>
                <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Update a hat</Link>
                <Link to="/hats/delete" className="btn btn-primary btn-lg px-4 gap-3">Delete a hat</Link>
              </div>
            </div>
          </div>
          <div className="container">
            <h2>Hats in your closet</h2>
            <div className="row">
              {hatColumns.map((hatList, index) => {
                return (
                  <HatColumn key={index} list={hatList} />
                );
              })}
            </div>
          </div>
        </>
      );  
  }

  export default MainPage
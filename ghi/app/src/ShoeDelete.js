import React, { useState } from "react";

function ShoeDelete() {
  const [shoeId, setShoeId] = useState("");
  const [isDeleted, setIsDeleted] = useState(false);

  const handleShoeIdChange = (event) => {
    setShoeId(event.target.value);
  };

  const handleDeleteShoe = async (event) => {
    event.preventDefault();

    try {
      // Send a DELETE request to delete the shoe with the specified ID
      const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}/`, {
        method: "DELETE",
      });

      if (response.ok) {
        // Shoe was successfully deleted
        setIsDeleted(true);
      } else {
        console.log("Could not delete shoe");
      }
    } catch (error) {
      console.error("Error deleting shoe:", error);
    }
  };

  return (
    <div>
      <h2>Delete a Shoe</h2>
      {isDeleted ? (
        <div>
          <p>Shoe with ID {shoeId} has been deleted.</p>
        </div>
      ) : (
        <form onSubmit={handleDeleteShoe}>
          <div className="mb-3">
            <label htmlFor="shoeId" className="form-label">
              Shoe ID:
            </label>
            <input
              type="text"
              className="form-control"
              id="shoeId"
              value={shoeId}
              onChange={handleShoeIdChange}
            />
          </div>
          <button type="submit" className="btn btn-danger">
            Delete Shoe
          </button>
        </form>
      )}
    </div>
  );
}

export default ShoeDelete;

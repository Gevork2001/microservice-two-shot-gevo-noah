# Wardrobify

Team:

- Person 1(Gevork) - Hats microservice?
- Person 2 (Noah)- Shoes microservice?

## Design

## Shoes microservice

The "shoes/api" is a Django application that encompasses URL patterns, Views, and Models files. Within "shoes/poll," there are Django resources for the RESTful API project, which are used to retrieve location data from the Wardrobe API by utilizing the Bin Data. The application runs on Port 8080 and handles various Shoe resources, including the manufacturer, model name, color, URL for an image of the shoe, and the location within the wardrobe, typically represented as a bin.

The functions of the Shoe app are as follows:

It interacts with a RESTful API to perform operations such as retrieving a list of shoes, creating a new shoe, and deleting an existing shoe.
Utilizes React to display a list of shoes and their details, offering an engaging user interface.
Employs React to provide a user-friendly form for creating new shoes.
Offers users the option to delete a shoe as needed.
Supports routing of existing navigation links to corresponding components, ensuring seamless navigation within the application.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
